import 'package:flutter/material.dart';

class MealColors {
  static const bluePrimary = Color(0xFF0682C2);
  static const bluePrimaryAccent = Color(0xFF0682C2);
  static const bluePrimaryDark = Color(0xFF056B9F);
  static const redSelection = Color(0xFFFF6B6B);
  static const lightGreyColor = Color(0xFFECEFF1);
  static const greenColor = Color(0xFF13ED1C);
  static const blueColor = Color(0xFF0075FF);
  static const yellowColor = Color(0xFFEDBD13);
  static const darkColor300 = Color(0xFF253858);
  static const homeFabGreen = Color(0xFF49D816);
  static const homeFabOrange = Color(0xFFF99B57);
  static const homeFabBlue = Color(0xFF55CEF5);
  static const homeFabMaroon = Color(0xFFF06292);
  static const colorGrey = Color(0xFF8993A4);
  static const blue300 = Color(0xFF3398CD);
  static const redLogin = Color(0xFFFF0000);
  static const primaryColorDark = Color(0xFF000000);
  static const primaryColor = Color(0xFF828282);
  static const grayPointColor = Color(0xFF333333);
  static const brownColor = Color(0xFF6B492A);
  static const brownColorSecondary = Color(0xFF864000);
  static const brownColor100 = Color(0xFFF3E9D1);
  static const yellowSecondaryColor = Color(0xFFE79029);
  static const ndcWhite = Color(0xFFFFFF80);
  static const greenColorDark = Color(0xFF008205);
  static const redColorDark = Color(0xFF820000);
  static const redColorDarkSecondary = Color(0xFF5b0000);
  static const yellowRating = Color(0xFFFFB800);
  static const greyColorSecondary = Color(0xFFb6b6b6);












}
