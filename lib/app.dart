import 'dart:io';
import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:path_provider/path_provider.dart';

class App {
  static App? _instance;
  final String? apiBaseURL;
  final String? appTitle;

  Dio? dio;
  Directory? appDocsDir;


  App.configure({
    this.apiBaseURL,
    this.appTitle
  }) {
    _instance = this;
  }

  factory App() {
    if (_instance == null) {
      throw UnimplementedError("App must be configured first.");
    }

    return _instance!;
  }

  Future<void> init() async {

    appDocsDir = await getApplicationDocumentsDirectory();

    dio = Dio(BaseOptions(
      baseUrl: apiBaseURL!,
      connectTimeout: const Duration(seconds: 60),
      receiveTimeout: const Duration(seconds: 60),
      contentType: Headers.jsonContentType,
      responseType: ResponseType.json,
    ));


    (dio!.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };

    dio!.interceptors.add(InterceptorsWrapper(
        onRequest:(options, handler){

          return handler.next(options); //continue

        },
        onResponse:(response,handler) {

          return handler.next(response); // continue

        },
        onError: (DioException e, handler) {

          if (e.response!.statusCode != null) {
            if (e.response!.statusCode == 400) {
            }
            // INFO : Kicking out user to login page when !authenticated
            if (e.response!.statusCode == 401) {

            }
          }
          return  handler.next(e);//continue

        }
    ));


  }
}