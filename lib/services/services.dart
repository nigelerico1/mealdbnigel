import 'package:dio/dio.dart';
import 'package:mealdb_project/app.dart';
import 'package:mealdb_project/db/app_db.dart';
import 'package:mealdb_project/model/items_model.dart';

class Services {
  final Dio? _dio = App().dio;

  Future<ItemsModel> fetchMeals({String? type}) async {
    try {
      Response response = await _dio!.get(
        "filter.php",
        queryParameters: {
          'c': type,
        },
      );

      if (response.statusCode == 200) {
        return ItemsModel.fromJson(response.data);
      }

      return ItemsModel.fromJson(response.data);
    } on DioException catch (e) {
      throw Exception(e.message);
    }
  }

  Future<ItemsModel> fetchDetails({String? id}) async {
    try {
      Response response = await _dio!.get(
        "lookup.php",
        queryParameters: {
          'i': id,
        },
      );

      if (response.statusCode == 200) {
        return ItemsModel.fromJson(response.data);
      }

      return ItemsModel.fromJson(response.data);
    } on DioException catch (e) {
      throw Exception(e.message);
    }
  }

  Future<ItemsModel> searchMeals({String? name}) async {
    try {
      Response response = await _dio!.get(
        "search.php",
        queryParameters: {
          's': name,
        },
      );

      if (response.statusCode == 200) {
        return ItemsModel.fromJson(response.data);
      }

      return ItemsModel.fromJson(response.data);
    } on DioException catch (e) {
      throw Exception(e.message);
    }
  }
}
