import 'dart:io';
import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:mealdb_project/db/entity/meals_entity.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:sqlite3/sqlite3.dart';
import 'package:sqlite3_flutter_libs/sqlite3_flutter_libs.dart';
part 'app_db.g.dart';

@DriftDatabase(tables: [FavoriteMeal])
class AppDb extends _$AppDb {
  AppDb() : super(_openConnection());

  @override
  int get schemaVersion => 1;


  Future<List<FavoriteMealData>> getFavoriteMeal() async {
    return await select(favoriteMeal).get();
  }

  Future<FavoriteMealData> getFavoriteMealById(int id) async {
    return await (select(favoriteMeal)..where((tbl) => tbl.id.equals(id))).getSingle();
  }

  Future<bool> updateFavoriteMeal(FavoriteMealCompanion entity) async {
    return await update(favoriteMeal).replace(entity);
  }

  Future<int> insertFavoriteMeal(FavoriteMealCompanion entity) async {
    return await into(favoriteMeal).insert(entity);
  }

  Future<int> deleteFavoriteMeal(int id) async {
    return await (delete(favoriteMeal)..where((tbl) => tbl.id.equals(id))).go();
  }
}



LazyDatabase _openConnection() {

  return LazyDatabase(() async {

    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'meals.sqlite'));

    if (Platform.isAndroid) {
      await applyWorkaroundToOpenSqlite3OnOldAndroidVersions();
    }

    final cachebase = (await getTemporaryDirectory()).path;

    sqlite3.tempDirectory = cachebase;

    return NativeDatabase.createInBackground(file);
  });
}

