import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealdb_project/cubit/bloc_observer.dart';
import 'package:mealdb_project/main_app.dart';

import 'app.dart';

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = MyBlocObserver();

  App.configure(
      apiBaseURL: 'https://www.themealdb.com/api/json/v1/1/',
      appTitle: 'Meal DB');

  await App().init();

  runApp(const MainApp());
}



