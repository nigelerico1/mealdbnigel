import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  int selectedPage = 0;


  void changeSelectedPage(int targetPage) {
    emit(ChangeMainPageInit());
    selectedPage = targetPage;
    emit(ChangedMainPage());
  }

}
