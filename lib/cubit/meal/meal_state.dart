part of 'meal_cubit.dart';

abstract class MealState extends Equatable {
  const MealState();
  @override
  List<Object> get props => [];
}

class MealInitial extends MealState {}

class GetDessertInitialState extends MealState {}

class GetDessertSuccessfulState extends MealState {
  final List<Meals> listDessert;

  const GetDessertSuccessfulState(this.listDessert);
}

class GetDessertFailedState extends MealState {
  final String? message;

  const GetDessertFailedState(this.message);
}


class GetSeaFoodInitialState extends MealState {}

class GetSeaFoodSuccessfulState extends MealState {
  final List<Meals> listSeafood;

  const GetSeaFoodSuccessfulState(this.listSeafood);
}

class GetSeaFoodFailedState extends MealState {
  final String? message;

  const GetSeaFoodFailedState(this.message);
}


