import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mealdb_project/db/app_db.dart';
import 'package:mealdb_project/model/items_model.dart';
import 'package:mealdb_project/services/services.dart';

part 'meal_state.dart';

class MealCubit extends Cubit<MealState> {
  MealCubit() : super(MealInitial());

  final Services _services = Services();


  void getDesserts() async {
    emit(GetDessertInitialState());

    ItemsModel apiResult = await _services.fetchMeals(type: 'Dessert');

    if (apiResult.meals!.isNotEmpty) {
      List<Meals>? listDessert = apiResult.meals;

      emit(GetDessertSuccessfulState(listDessert ?? []));
    } else {
      emit(const GetDessertFailedState("Empty Product"));
    }
  }


  void getSeaFood() async {
    emit(GetSeaFoodInitialState());

    ItemsModel apiResult = await _services.fetchMeals(type: 'Seafood');

    if (apiResult.meals!.isNotEmpty) {
      List<Meals>? listSeaFood = apiResult.meals;

      emit(GetSeaFoodSuccessfulState(listSeaFood ?? []));
    } else {
      emit(const GetSeaFoodFailedState("Empty Product"));
    }
  }


}
