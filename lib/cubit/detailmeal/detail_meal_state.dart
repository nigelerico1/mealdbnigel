part of 'detail_meal_cubit.dart';

abstract class DetailMealState extends Equatable {
  const DetailMealState();
  @override
  List<Object> get props => [];
}

class DetailMealInitial extends DetailMealState {

}


class GetDetailMealInitialState extends DetailMealState {}

class GetDetailMealSuccessfulState extends DetailMealState {
  final List<Meals> detailMeal;

  const GetDetailMealSuccessfulState(this.detailMeal);
}

class GetDetailMealFailedState extends DetailMealState {
  final String? message;

  const GetDetailMealFailedState(this.message);
}

