import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mealdb_project/model/items_model.dart';
import 'package:mealdb_project/services/services.dart';

part 'detail_meal_state.dart';

class DetailMealCubit extends Cubit<DetailMealState> {
  DetailMealCubit() : super(DetailMealInitial());

  final Services _services = Services();

  void getDetailMeal({required String id}) async {
    emit(GetDetailMealInitialState());

    ItemsModel apiResult = await _services.fetchDetails(id: id);

    if (apiResult.meals!.isNotEmpty) {
      List<Meals>? detailMeal = apiResult.meals;

      emit(GetDetailMealSuccessfulState(detailMeal ?? []));
    } else {
      emit(const GetDetailMealFailedState("Empty Product"));
    }
  }
}
