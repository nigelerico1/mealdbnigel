import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mealdb_project/db/app_db.dart';
import 'package:mealdb_project/model/items_model.dart';
import 'package:mealdb_project/ui/page/detail_meal_page.dart';
import 'package:mealdb_project/utils/colors.dart';

class ListProducts extends StatelessWidget {
  final VoidCallback onTap;
  final Meals data;
  const ListProducts({super.key, required this.data, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Card(
          elevation: 3,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                  ),
                  image: DecorationImage(
                      image:
                      NetworkImage(data.strMealThumb ?? ""),
                      fit: BoxFit.fill),
                ),
                width: MediaQuery.of(context).size.width,
                height: 160.h,
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: data.strMeal ?? "",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: MealColors.darkColor300,
                                      fontSize: 14.sp),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
