import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:mealdb_project/cubit/meal/meal_cubit.dart';
import 'package:mealdb_project/model/items_model.dart';
import 'package:mealdb_project/ui/page/detail_meal_page.dart';
import 'package:mealdb_project/ui/widgets/list_products.dart';
import 'package:mealdb_project/utils/colors.dart';
import 'package:mealdb_project/utils/show_flutter_toast.dart';

class SeafoodPage extends StatefulWidget {
  const SeafoodPage({super.key});

  @override
  State<SeafoodPage> createState() => _SeafoodPageState();
}

class _SeafoodPageState extends State<SeafoodPage> {

  late MealCubit _mealCubit = MealCubit();

  @override
  void initState() {
    _mealCubit = BlocProvider.of<MealCubit>(context);
    _mealCubit.getSeaFood();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Seafood",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: BlocListener<MealCubit, MealState>(
        bloc: _mealCubit,
        listener: (context, state) {
          if (state is GetSeaFoodFailedState) {
            showFlutterToast(state.message ?? "Terjadi Kesalahan");
          }
        },
        child: BlocBuilder<MealCubit, MealState>(
          bloc: _mealCubit,
          builder: (context, state) {
            if (state is GetSeaFoodInitialState) {
              return Center(
                child: LoadingAnimationWidget.twistingDots(
                  leftDotColor: const Color(0xFF1A1A3F),
                  rightDotColor: MealColors.bluePrimary,
                  size: 40,
                ),
              );
            } else if (state is GetSeaFoodSuccessfulState) {
              double width = MediaQuery.of(context).size.width;
              double itemWidth = (width / 2.3) - 32;
              double itemHeight = itemWidth + 105;
              return GridView.builder(
                  padding: const EdgeInsets.all(8),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      crossAxisCount: 2,
                      mainAxisExtent: itemHeight
                  ),
                  itemCount: state.listSeafood.length,
                  itemBuilder: (context, int index) {
                    Meals data = state.listSeafood[index];
                    return ListProducts(data: data, onTap:  () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  DetailMealPage(idMeal: data.idMeal ?? "", strMeal: data.strMeal ?? "",
                                      strMealThumb: data.strMealThumb ?? "")));
                    },);
                  });
            } else {
              return Container();
            }
          },
        ),
      ),

    );
  }
}

