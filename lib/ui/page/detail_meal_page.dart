import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:mealdb_project/cubit/detailmeal/detail_meal_cubit.dart';
import 'package:mealdb_project/db/app_db.dart';
import 'package:mealdb_project/utils/colors.dart';
import 'package:mealdb_project/utils/show_flutter_toast.dart';
import 'package:drift/drift.dart' as drift;
import 'package:provider/provider.dart';

class DetailMealPage extends StatefulWidget {
  final String idMeal;
  final String strMeal;
  final String strMealThumb;


  const DetailMealPage(
      {super.key, required this.idMeal, required this.strMeal, required this.strMealThumb});

  @override
  State<DetailMealPage> createState() => _DetailMealPageState();
}

class _DetailMealPageState extends State<DetailMealPage> {

  late DetailMealCubit _detailMealCubit = DetailMealCubit();
  bool _isFavorite = false;

  @override
  void initState() {
    _detailMealCubit = BlocProvider.of<DetailMealCubit>(context);
    _detailMealCubit.getDetailMeal(id: widget.idMeal);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<AppDb>(context, listen: false).getFavoriteMealById(int.parse(widget.idMeal)).then((value) {
        if(value.strMeal != null){
          setState(() => _isFavorite = value.id == int.parse(widget.idMeal));
        }
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              title: Text(
                widget.strMeal,
              ),
              expandedHeight: 250,
              floating: false,
              pinned: true,
              leading: IconButton(
                key: const Key('back'),
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              actions: <Widget>[
                actionSaveOrDelete()
              ],
              flexibleSpace: FlexibleSpaceBar(
                background: Hero(
                  tag: widget.strMeal,
                  child: Image.network(
                      widget.strMealThumb, width: double.infinity,
                      fit: BoxFit.cover),
                ),
              ),
            ),
          ];
        },
        body: BlocListener<DetailMealCubit, DetailMealState>(
          bloc: _detailMealCubit,
          listener: (context, state) {
            if (state is GetDetailMealFailedState) {
              showFlutterToast(state.message ?? "Terjadi Kesalahan");
            }
          },
          child: BlocBuilder<DetailMealCubit, DetailMealState>(
            bloc: _detailMealCubit,
            builder: (context, state) {
              if (state is GetDetailMealInitialState) {
                return Center(
                  child: LoadingAnimationWidget.twistingDots(
                    leftDotColor: const Color(0xFF1A1A3F),
                    rightDotColor: MealColors.bluePrimary,
                    size: 40,
                  ),
                );
              } else if (state is GetDetailMealSuccessfulState) {
                return Container(
                  padding: const EdgeInsets.all(16.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.all(4.0),
                          child: Column(
                            children: <Widget>[
                              const Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Descriptive :",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  state.detailMeal[0].strInstructions ?? "",
                                  style: const TextStyle(color: Colors.black),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return Container();
              }
            },
          ),
        ),
      ),
    );
  }

  Widget actionSaveOrDelete() {
    if (_isFavorite) {
      return GestureDetector(
        child: const Padding(
          padding: EdgeInsets.all(16.0),
          child: Icon(Icons.favorite),
        ),
        onTap: () {
          Provider.of<AppDb>(context,listen: false).deleteFavoriteMeal(int.parse(widget.idMeal)).then((value) {
            if (value > 0) {
              setState(() => _isFavorite = false);
            }
          });;
        },
      );
    } else {
      return GestureDetector(
        onTap: () {
          final entity = FavoriteMealCompanion(
              id: drift.Value(int.parse(widget.idMeal)),
              strMeal: drift.Value(widget.strMeal),
              strMealThumb: drift.Value(widget.strMealThumb)
          );

          // _db.insertFavoriteMeal(entity).then((value) => ScaffoldMessenger.of(context).
          // showMaterialBanner(
          //   MaterialBanner(
          //     backgroundColor: MealColors.primaryColorDark,
          //       content: Text("New Favorite Meal Insert $value", style: const TextStyle(color: Colors.white),),
          //       actions: [
          //         TextButton(
          //             onPressed: () => ScaffoldMessenger.of(context).hideCurrentMaterialBanner(),
          //             child: const Text('Close'))
          //       ])
          // ));
          Provider.of<AppDb>(context, listen: false).insertFavoriteMeal(entity).then((value) {
            if (value > 0) {
              setState(() => _isFavorite = true);
            }
          });
        },
        child: const Padding(
          padding: EdgeInsets.all(16.0),
          child: Icon(Icons.favorite_border),
        ),
      );
    }
  }

}
