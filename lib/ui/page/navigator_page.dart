import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mealdb_project/cubit/home/home_cubit.dart';
import 'package:mealdb_project/ui/page/dessert/dessert_page.dart';
import 'package:mealdb_project/ui/page/favorite/favorite_page.dart';
import 'package:mealdb_project/ui/page/seafood/seafood_page.dart';
import 'package:mealdb_project/utils/colors.dart';

class NavigatorPage extends StatefulWidget {
  const NavigatorPage({super.key});

  @override
  State<NavigatorPage> createState() => _NavigatorPageState();
}

class _NavigatorPageState extends State<NavigatorPage> {
  late HomeCubit _homeCubit;
  List<Widget> _pages = [];

  @override
  void initState() {
    _homeCubit = BlocProvider.of<HomeCubit>(context);
    _pages = _getPages();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: _homeCubit,
      builder: (context, state) {
        return Scaffold(
          body: _pages[_homeCubit.selectedPage],
          bottomNavigationBar: Container(
            decoration:
            const BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Color(0x1A000000),
                offset: Offset(0, -3),
                blurRadius: 20,
              ),
            ]),
            child: BottomNavigationBar(
              currentIndex: _homeCubit.selectedPage,
              type: BottomNavigationBarType.fixed,
              showUnselectedLabels: true,
              showSelectedLabels: true,
              items: _getMenuItems(),
              selectedLabelStyle: const TextStyle(
                  fontWeight: FontWeight.w600, color: Colors.black),
              unselectedItemColor: const Color.fromRGBO(125, 125, 125, 0.4),
              selectedItemColor: MealColors.primaryColor,
              onTap: (int index) => _homeCubit.changeSelectedPage(index),
            ),
          ),
        );
      },
    );
  }

  List<Widget> _getPages() {
    List<Widget> pages = [
      const DesertPage(),
      const SeafoodPage(),
      const FavoritePage(),
    ];

    return pages;
  }

  List<BottomNavigationBarItem> _getMenuItems() {
    List<BottomNavigationBarItem> mn = [
      BottomNavigationBarItem(
        activeIcon: ShaderMask(
          shaderCallback: (Rect bounds) {
            return const RadialGradient(
              center: Alignment.topCenter,
              radius: 1,
              colors: <Color>[MealColors.primaryColorDark, Colors.white],
              tileMode: TileMode.mirror,
            ).createShader(bounds);
          },
          child: const FaIcon(FontAwesomeIcons.cakeCandles),
        ),
        icon: const FaIcon(FontAwesomeIcons.cakeCandles),
        label: 'Dessert',
      ),
      BottomNavigationBarItem(
        activeIcon: ShaderMask(
          shaderCallback: (Rect bounds) {
            return const RadialGradient(
              center: Alignment.topCenter,
              radius: 1,
              colors: <Color>[MealColors.primaryColorDark, Colors.white],
              tileMode: TileMode.mirror,
            ).createShader(bounds);
          },
          child: const FaIcon(FontAwesomeIcons.fish),
        ),
        icon: const FaIcon(FontAwesomeIcons.fish),
        label: 'Seafood',
      ),
      BottomNavigationBarItem(
          activeIcon: ShaderMask(
            shaderCallback: (Rect bounds) {
              return const RadialGradient(
                center: Alignment.topCenter,
                radius: 1,
                colors: <Color>[
                  MealColors.primaryColorDark,
                  Colors.white
                ],
                tileMode: TileMode.mirror,
              ).createShader(bounds);
            },
            child: const FaIcon(FontAwesomeIcons.star),
          ),
          icon: const FaIcon(FontAwesomeIcons.star),
          label: 'Favorite'),
    ];

    return mn;
  }


}
