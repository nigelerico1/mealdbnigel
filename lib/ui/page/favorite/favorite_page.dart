import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:mealdb_project/db/app_db.dart';
import 'package:mealdb_project/model/items_model.dart';
import 'package:mealdb_project/ui/page/detail_meal_page.dart';
import 'package:mealdb_project/ui/widgets/list_products.dart';
import 'package:mealdb_project/utils/colors.dart';
import 'package:provider/provider.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({super.key});

  @override
  State<FavoritePage> createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Favorite",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: FutureBuilder<List<FavoriteMealData>>(
          future: Provider.of<AppDb>(context, listen: false).getFavoriteMeal(),
          builder: (context, snapshot) {
            final List<FavoriteMealData>? meal = snapshot.data;
            if (snapshot.connectionState != ConnectionState.done) {
              return Center(
                child: LoadingAnimationWidget.twistingDots(
                  leftDotColor: const Color(0xFF1A1A3F),
                  rightDotColor: MealColors.bluePrimary,
                  size: 40,
                ),
              );
            }
            if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            }
            if (meal != null) {
              double width = MediaQuery.of(context).size.width;
              double itemWidth = (width / 2.3) - 32;
              double itemHeight = itemWidth + 105;
              return GridView.builder(
                  padding: const EdgeInsets.all(8),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      crossAxisCount: 2,
                      mainAxisExtent: itemHeight),
                  itemCount: meal.length,
                  itemBuilder: (context, int index) {
                    FavoriteMealData data = meal[index];
                    return ListProducts(
                      data: Meals(
                          idMeal: data.id.toString(),
                          strMeal: data.strMeal,
                          strMealThumb: data.strMealThumb),
                      onTap: () {
                        Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailMealPage(
                                        idMeal: data.id.toString() ?? "",
                                        strMeal: data.strMeal ?? "",
                                        strMealThumb: data.strMealThumb ?? "")))
                            .then((value) {
                          setState(() {});
                        });
                      },
                    );
                  });
            }
            return const Text("No Data Found");
          },
        ));
  }
}
