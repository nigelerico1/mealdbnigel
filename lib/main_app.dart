import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mealdb_project/app.dart';
import 'package:mealdb_project/cubit/detailmeal/detail_meal_cubit.dart';
import 'package:mealdb_project/cubit/home/home_cubit.dart';
import 'package:mealdb_project/cubit/meal/meal_cubit.dart';
import 'package:mealdb_project/db/app_db.dart';
import 'package:mealdb_project/ui/page/navigator_page.dart';
import 'package:mealdb_project/utils/colors.dart';
import 'package:provider/provider.dart';

class MainApp extends StatelessWidget {
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    MealCubit mealCubit = MealCubit();
    HomeCubit homeCubit = HomeCubit();
    DetailMealCubit detailMealCubit = DetailMealCubit();


    return Provider(
      create: (context) => AppDb(),
      dispose: (context, AppDb db) => db.close(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider<MealCubit>(create: (context) => mealCubit),
          BlocProvider<HomeCubit>(create: (context) => homeCubit),
          BlocProvider<DetailMealCubit>(create: (context) => detailMealCubit),
        ],
        child: ScreenUtilInit(
          designSize: const Size(360, 720),
          builder: (BuildContext context, child) => MaterialApp(
              navigatorKey: navigatorKey,
              debugShowCheckedModeBanner: false,
              title: App().appTitle!,
              theme: ThemeData(
                useMaterial3: false,
                appBarTheme: const AppBarTheme(color: MealColors.primaryColorDark),
                primaryColor: MealColors.primaryColorDark,
                primaryColorDark: MealColors.primaryColorDark,
                visualDensity: VisualDensity.adaptivePlatformDensity,
                brightness: Brightness.light,
                textSelectionTheme: const TextSelectionThemeData(
                  cursorColor: MealColors.primaryColor,
                  selectionColor: MealColors.primaryColor,
                  selectionHandleColor: MealColors.primaryColor,
                ),
              ),
              home: const NavigatorPage()),
        ),
      ),
    );
  }
}
